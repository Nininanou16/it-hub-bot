
# IT-hub-bot

The open source & powerful bot written for the french tech/developement discord server IT Hub -> https://discord.gg/3b22WyU

**:flag_fr: Comment installer & demarrer le bot**

 Vous devez ensuite cloner le bot a partir de gitlab avec la commande suivante (git est requis) :
`git clone https://gitlab.com/Nininanou16/it-hub-bot.git`

Vous devez ensuite creer le fichier settings.json, en suivant le modele du fichier settings.example.json.

> Pour obtenir le token de votre bot, vous devez aller sur [votre panel discord developer](https://discord.com/developers/applications), puis cliquer sur le bouton "new application", si vous n'avez pas deja de bot. Allez ensuite dans l'onglet "Bot", a droite, puis cliquez sur "Add Bot". Vous avez ensuite, tout en haut de la page, sous le pseudo de votre bot le bouton "Copy", il vous suffit de cliquer dessus, puis de coller votre token dans le fichier de configuration.

Vous devez ensuite installer les packages necesssaires au bot :
`npm i`

vous pouvez ensuite demarrer le bot :
`node .`

> **Note :**
> Pour un fonctionnement correct du bot, vous devez diposer d'au moins NodeJS 12. Pour verifier la version veuillez executer cette commande dans un terminal/invite de commande :
> `node --version`
> Si la version indiquee est inferieure a 12, veuillez [telecharger une nouvelle version de NodeJS](https://nodejs.org/fr/download/)

---

**:flag_gb: How to install & start the bot**

You must clone the bot's code from the gitlab repository :
`git clone https://gitlab.com/Nininanou16/it-hub-bot.git`

You must then create the settings.json file, following the template of the settings.example.json file.

> In order to get you bot's toke, you must go on your [discord developer dashboard](https://discord.com/developers/applications), and clic if not already done on the "New Application" button. Then go in the "Bot" tab on the left, and clic on "Add bot". You must then at the top of the page, under your bot's username click on the "Copy" button, and paste the token in your configuration file

You must then install all the needed packages for the bot :
`npm i`

You can then start the bot :
`node .`

> **Note :**
> To get the bot working well, you must have NodeJS 12 or newer. To verify your version please type in a terminal the following command :
> `node --version`
> If the showed version is under 12, or if you don't have NodeJS installed, please [download it](https://nodejs.org/en/download/)
-------
By Nininanou16
