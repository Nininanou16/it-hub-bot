const lyrics = require('lyrics-parse')
const Discord = require('discord.js')
const ms = require('ms')

module.exports = {
  name: 'lyrics',
  aliases: [],
  category: 'music',
  run: async (client, msg, args) => {
    let clientMember = msg.guild.member(client.user);

    if (!clientMember.voice.channel) return msg.channel.send(
      new Discord.MessageEmbed()
        .setColor('4e3f7b')
        .setDescription(':speaker: | Je ne suis pas entrain de jouer de musique actuellement, utilisez la commande `play` pour en ajouter une !')
    )

    let music = client.queue.get(msg.guild.id);

    if (!music) return msg.channel.send(
      new Discord.MessageEmbed()
        .setColor('4e3f7b')
        .setDescription(':speaker: | Je ne suis pas entrain de jouer de musique actuellement, utilisez la commande `play` pour en ajouter une !')
    )

    let currentMusic = music.songs[0];

    if (!currentMusic) return msg.channel.send(
      new Discord.MessageEmbed()
        .setColor('4e3f7b')
        .setDescription(':speaker: | Je ne suis pas entrain de jouer de musique actuellement, utilisez la commande `play` pour en ajouter une !')
    )

    let songLyrics = await lyrics(currentMusic.title, currentMusic.author.name);

    if (!songLyrics) return msg.channel.send(
      new Discord.MessageEmbed()
        .setColor('4e3f7b')
        .setDescription(':x: | Je n\'ai trouvé aucune paroles pour la musique actuelle !')
    )

    if (songLyrics.length >= 2000) {
      let pages = []
      for (let i = 0; i <= Math.ceil(parseInt(songLyrics.length)/2000); i++) {
        pages.push(songLyrics.substring(0, 2000))
        songLyrics = songLyrics.substring(2000);
      }

      let currentPage = 0

      let embed = new Discord.MessageEmbed()
        .setColor('1a356f')
        .setTitle(`Paroles de : ${currentMusic.title} (1/${pages.length})`)
        .setDescription(pages[0])

      let menu = await msg.channel.send(embed);
        menu.react('◀️');
        menu.react('▶️');

      let reactions = ['◀️','▶️']

      let filter = (reaction, user) => {
        return reactions.includes(reaction.emoji.name) && user.id == msg.author.id
      }

      async function update() {
        embed.setTitle(`Paroles de : ${currentMusic.title} (${currentPage+1}/${pages.length})`)
          .setDescription(pages[currentPage])

        menu.edit(embed)
      }

      let collector = menu.createReactionCollector(filter, { time: ms('10m') })
      collector.on('collect', (reaction, user) => {
        switch(reaction.emoji.name) {
          case '◀️':
          console.log('-1')
            currentPage = currentPage-1
            if (currentPage < 0) currentPage = pages.lenght-1
            update()
          break

          case '▶️':
            console.log('+1')
            currentPage = currentPage+1
            if (currentPage+1 > pages.lenght) currentPage = 0
            update()
          break
        }
      })
    } else {
      msg.channel.send(
        new Discord.MessageEmbed()
          .setColor('1a356f')
          .setTitle(`Paroles de : ${currentMusic.title}`)
          .setDescription(songLyrics)
      )
    }
  }
}


//.substring()
