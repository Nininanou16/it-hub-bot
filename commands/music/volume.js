const Discord = require('discord.js')

module.exports = {
  name: 'volume',
  aliases: ['vl', 'vol'],
  category: 'music',
  run: async (client, msg, args) => {
    let clientMember = msg.guild.member(client.user);

    if (!clientMember.voice.channel) return msg.channel.send(
      new Discord.MessageEmbed()
        .setColor('4e3f7b')
        .setDescription(':speaker: | Je ne suis pas entrain de jouer de musique actuellement, utilisez la commande `play` pour en ajouter une !')
    )

    if (!msg.member.voice.channel || msg.member.voice.channel !== clientMember.voice.channel) return msg.channel.send(
      new Discord.MessageEmbed()
        .setColor('4e3f7b')
        .setDescription(':mag: | Vous n\'êtes pas dans le salon vocal musical. Vous ne pouvez pas utiliser cette commande !')
    )

    let guildQueue = client.queue.get(msg.guild.id);

    let emojis = ['🔈','🔉','🔊'];

    let sound = parseInt(args[0]);

    if (!sound || sound > 100) return msg.channel.send(
      new Discord.MessageEmbed()
        .setColor('4e3f7b')
        .setDescription(':x: | Le volume indiqué est invalide. Veuillez reessayer avec un volume compris entre 0 et 100.')
    );

    guildQueue.player.setVolumeLogarithmic(sound/100);

    guildQueue.volume = sound/100

    let emoji = emojis[Math.ceil((sound/100)*3-1)];

    msg.channel.send(
      new Discord.MessageEmbed()
        .setColor('1a356f')
        .setDescription(`${emoji} | Le volume est maintenant a \`${sound}%\` !`)
    )
  }
}
