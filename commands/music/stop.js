const Discord = require('discord.js')

module.exports = {
  name: 'stop',
  aliases: [],
  category: 'music',
  run: async (client, message, args) => {
    let clientMember = message.guild.member(client.user);

    if (!clientMember.voice.channel) return message.channel.send(
      new Discord.MessageEmbed()
        .setColor('4e3f7b')
        .setDescription(':speaker: | Je ne suis pas entrain de jouer de musique actuellement, utilisez la commande `play` pour en ajouter une !')
    )

    if (!message.member.voice.channel || message.member.voice.channel !== clientMember.voice.channel) return msg.channel.send(
      new Discord.MessageEmbed()
        .setColor('4e3f7b')
        .setDescription(':mag: | Vous n\'êtes pas dans le salon vocal musical. Vous ne pouvez pas utiliser cette commande !')
    )

    client.functions.stop(client, message.guild);

    message.channel.send(
      new Discord.MessageEmbed()
        .setColor('1a356f')
        .setDescription(':stop_button: | La musique a bien été arrêtée. Si vous souhaitez jouer un morceau a nouveau, utilisez la commande `play`')
    )
  }
}
