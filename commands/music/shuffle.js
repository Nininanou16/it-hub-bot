const Discord = require('discord.js')

module.exports = {
  name: 'shuffle',
  aliases: [],
  category: 'music',
  run: async (client, msg, args) => {
    let clientMember = msg.guild.member(client.user);

    if (!clientMember.voice.channel) return msg.channel.send(
      new Discord.MessageEmbed()
        .setColor('4e3f7b')
        .setDescription(':speaker: | Je ne suis pas entrain de jouer de musique actuellement, utilisez la commande `play` pour en ajouter une !')
    )

    if (!msg.member.voice.channel || msg.member.voice.channel !== clientMember.voice.channel) return msg.channel.send(
      new Discord.MessageEmbed()
        .setColor('4e3f7b')
        .setDescription(':mag: | Vous n\'êtes pas dans le salon vocal musical. Vous ne pouvez pas utiliser cette commande !')
    )

    let guildQueue = client.queue.get(msg.guild.id)

    let queue = guildQueue.songs
    let song = queue[0]
    queue.shift()
    queue = shuffle(queue);
    queue.unshift(song);

    guildQueue.songs = queue;
    client.queue.set(msg.guild.id, guildQueue)

    msg.channel.send(
      new Discord.MessageEmbed()
        .setColor('1a356f')
        .setDescription(':twisted_rightwards_arrows: | L\'ordre des musiques de la file de lecture a été réorganisé de manirère aléatoire !')
    )
  }
}

function shuffle(a) {
    var j, x, i;
    for (i = a.length - 1; i > 0; i--) {
        j = Math.floor(Math.random() * (i + 1));
        x = a[i];
        a[i] = a[j];
        a[j] = x;
    }
    return a;
}
