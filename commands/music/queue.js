const Discord = require('discord.js');

module.exports = {
  name: 'queue',
  aliases: ['q'],
  category: 'music',
  run: async (client, msg, args) => {
    let clientMember = msg.guild.member(client.user);

    if (!clientMember.voice.channel) return msg.channel.send(
      new Discord.MessageEmbed()
        .setColor('4e3f7b')
        .setDescription(':speaker: | Je ne suis pas entrain de jouer de musique actuellement, utilisez la commande `play` pour en ajouter une !')
    )

    if (!msg.member.voice.channel || msg.member.voice.channel !== clientMember.voice.channel) return msg.channel.send(
      new Discord.MessageEmbed()
        .setColor('4e3f7b')
        .setDescription(':mag: | Vous n\'êtes pas dans le salon vocal musical. Vous ne pouvez pas utiliser cette commande !')
    )

    let guildQueue = client.queue.get(msg.guild.id)
    let queue = guildQueue.songs;

    let currentSong = queue[0]

    //if (queue.lenght > 6) queue.lenght = 6
    let txt = '';

    let pages = []

    let count = 0;

    let page = 0

    for (i in queue) {
      count++
      console.log(i)
      console.log(count)
      if (count > 6) {
        console.log(txt)
        pages.push(txt);
        txt = '';
        count = 1
      }
      console.log('adding text')
      let song = guildQueue.songs[i];
      switch (i) {
        case '0':
          console.log('zero')
          let now = `**:arrow_forward: | En cours de lecture : \`${song.title}\` (${song.duration})**\n\n`
        break;
        case '1':
          console.log('one')
          txt += `:one: - \`${song.title}\` (${song.duration}) | Demandé par \`${song.addedBy.tag}\`\n\n`
        break;
        case '2':
          console.log('two')
          txt += `:two: - \`${song.title}\` (${song.duration}) | Demandé par \`${song.addedBy.tag}\`\n\n`
        break;
        case '3':
          console.log('three')
          txt += `:three: - \`${song.title}\` (${song.duration}) | Demandé par \`${song.addedBy.tag}\`\n\n`
        break;
        default:
          console.log('normal')
          txt += `:radio_button: - \`${song.title}\` (${song.duration}) | Demandé par \`${song.addedBy.tag}\`\n\n`
        break;
      }
    }

    pages.push(txt)

    let embed = new Discord.MessageEmbed()
      .setColor('1a356f')
      .setTitle('Voici les prochaines morceaux de la liste de lecture :')
      .setDescription(`**:arrow_forward: | En cours de lecture : \`${currentSong.title}\` (${currentSong.duration})**\n\n` + (pages[0] || 'Aucune musique suplémentaires. Ajoutez en avec `play` :wink:'))

    let menu = await msg.channel.send(embed)

    console.log(pages)

    if (pages.length > 1) {
      menu.react('◀️');
      menu.react('▶️');

      let emojis = ['◀️','▶️'];
      let filter = (reaction, user) => {
        return emojis.includes(reaction.emoji.name) && user.id == msg.author.id
      }

      const collector = menu.createReactionCollector(filter, { time: 30000 })

      collector.on('collect', (reaction, user) => {
        reaction.users.remove(user)
        switch (reaction.emoji.name) {
          case '◀️':
            page--;
            if (page < 0) page = pages.length-1;
            update()
          break;

          case '▶️':
            page++;
            if (page > (pages.length-1)) page = 0;
            update()
          break;
        }
      })

      function update() {
        menu.edit(
          embed.setDescription(`**:arrow_forward: | En cours de lecture : \`${currentSong.title}\` (${currentSong.duration})**\n\n` + pages[page])
        )
      }
    }
  }
}
