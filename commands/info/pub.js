const Discord = require("discord.js")
const pub = `
\`\`\`
                        ⇉⇉⇉   🌐 ►𝗜𝗧 𝗛𝘂𝗯   ⇇⇇⇇
\`\`\`

**Un serveur Discord centré sur l'informatique, pour le partage, l'entraide et le dépannage !**

【:thumbsup:】Un STAFF **à l'écoute,  dispos et actif** qui sait mettre la bonne humeur !

【:bookmark_tabs:】Des **vidéos** diverses **sur l'informatique** et des **sondages** réguliers !

【:wrench:】Des **salons de dépannage** avec du **personnel qualifié** pour vous aider !

【:desktop:】Une communauté de **développeurs** active et diversifiée !

【:gem:】Divers moyen de **faire sa pub** et de **mettre en valeur ses projets** !

【:tada:】Un système d'**évents variés et amusants** !

:pushpin: **Ton Ticket :** https://discord.gg/9zQFpy6DtW

\`\`\`

\`\`\`
`

module.exports = {
  name: "pub",
  aliases: [],
  category: "info",
  run: async (client, message, args) => {
    message.channel.send(new Discord.MessageEmbed().setDescription(`Voici la publicité du serveur :\n${pub}\n\nPublicité copiable :\n\`${pub.replace("`", "\`")}\``))
  }
}

/*
    This file is part of It-Hub Bot.

    It-Hub Bot is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    It-Hub Bot is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with It-Hub Bot.  If not, see <https://www.gnu.org/licenses/>.

*/
