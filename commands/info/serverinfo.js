const { ChartJSNodeCanvas } = require("chartjs-node-canvas");
const db = require('quick.db');
const Discord = require('discord.js');
const ms = require('ms')

module.exports = {
  name: 'serverinfo',
  aliases: ['si','serveri','sinfo'],
  category: 'info',
  run: async (client, msg, args) => {
    let servMembers = db.get(`members.${msg.guild.id}`);

    let fr = 'fr-FR'

    let dates = [];

    for (let i = 1; i<8; i++) {
      let date = new Date();
      date.setDate(date.getDate()-i)
      date = date.toLocaleString('fr-FR')
      dates.unshift(date.split(' ')[0])
    }

    const width = 800;
    const height = 300;
    const canvasRenderService = new ChartJSNodeCanvas({width, height});
    const ticksOptions = [{ ticks: { fontColor: "white", fontStyle: "bold" } }];
    const options = {
        // Hide legend
        legend: { display: false },
        scales: { yAxes: ticksOptions, xAxes: ticksOptions }
    };

    const image = await canvasRenderService.renderToBuffer({
      type: "line",
      data: {
        labels: dates,
        datasets: [
          {
            label: "Nombre de membres depuis 7 jours",
            data: db.get(`members.${msg.guild.id}`),
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: "rgb(61,148,192)",
            fill: true,
            backgroundColor: "rgba(61,148,192,0.1)"
          }
        ]
      },
      options
    })

    let created = msg.guild.createdTimestamp
    let diff = ms(ms((Date.now()-created).toString()))

    let attachment = new Discord.MessageAttachment(image, 'members.png');

    let embed = new Discord.MessageEmbed()
      .setColor('1f6996')
      .setTitle(`Informations du serveur ${msg.guild.name}`)
      .setDescription(`
        :busts_in_silhouette: Membres : ${msg.guild.memberCount}
        :level_slider: Salons : ${msg.guild.channels.cache.size}
        :pushpin: Roles : ${msg.guild.roles.cache.size}
        :grinning: Emojis : ${msg.guild.emojis.cache.size}
        :crown: Propriétaire : \`${msg.guild.owner.user.tag}\`
        :crystal_ball: Niveau de boost : ${msg.guild.premiumTier} (${msg.guild.premiumSubscriptionCount} boosts)
        :earth_africa: Région du serveur : ${msg.guild.region}
        :date: Date de création du serveur : ${msg.guild.createdAt.toLocaleString('fr-FR')} (${diff})

        Évolution des membres les 7 derniers jours :`)
      .attachFiles(attachment)
      .setImage('attachment://members.png');

      msg.channel.send(embed);
  }
}

/*
    This file is part of It-Hub Bot.

    It-Hub Bot is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    It-Hub Bot is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with It-Hub Bot.  If not, see <https://www.gnu.org/licenses/>.

*/
