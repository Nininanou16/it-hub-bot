const Discord = require('discord.js')

module.exports = (client, oldMsg, newMsg) => {
  let logs = client.channels.cache.get(client.settings.logsChannelId);
  if (!logs) return;
  if (newMsg.channel.id !== logs.guild.id)
  if (oldMsg.content !== newMsg.content) {
    logs.send(
      new Discord.MessageEmbed()
        .setColor('#1f6996')
        .setTitle('Modification de message')
        .setDescription(`Auteur du message : ${newMsg.author}
                        Salon du message : <#${newMsg.channel.id}>`)
        .addField('Ancien contenu du message :', oldMsg.content)
        .addField('Nouveau contenu du message :', newMsg.content)
    )
  }
}
