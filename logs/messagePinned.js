const Discord = require('discord.js')

module.exports = async (client, message) => {
  let logs = client.channels.cache.get(client.settings.logsChannelId);
  if (!logs) return;

  let guildLogs = await message.guild.fetchAuditLogs()
  guildLogs = guildLogs.entries.first();

  let pinner;

  if (guildLogs.action !== 'MESSAGE_PIN') pinner = { tag: 'Inconnu' };
  if (!pinner) pinner = guildLogs.executor

  let embed = new Discord.MessageEmbed()
    .setColor('1a356f')
    .setDescription(`
      **Nouveau message epinglé :**

      Salon du message : <#${message.channel.id}>
      Message epinglé par : \`${pinner.tag}\`
      [Lien vers le message](${message.url})`)
    .setTimestamp()

    logs.send(embed)
}

/*
    This file is part of It-Hub Bot.

    It-Hub Bot is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    It-Hub Bot is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with It-Hub Bot.  If not, see <https://www.gnu.org/licenses/>.

*/
