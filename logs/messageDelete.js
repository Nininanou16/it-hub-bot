const db = require('quick.db')
const Discord = require('discord.js')

module.exports = async (client, message) => {
  if (!message.author) return;
  if (message.author.id == client.user.id && message.channel.id == client.settings.logsChannelId) {
    let channel = client.channels.cache.get(client.settings.logsChannelId);

    if (!channel) return;

    channel.send(message.embeds);

    return;
  }

  if (!message.content && !message.attachments.first()) return;

  let logs = await message.guild.fetchAuditLogs()

  logs = logs.entries.first();

  let deleter;

  if (logs.action !== 'MESSAGE_DELETE') deleter = message.author;
  if (!deleter) deleter = logs.executor;

  let embed = new Discord.MessageEmbed()
    .setColor('#1f6996')
    .setDescription(`**Suppression de message :**\n\nAuteur du message : ${message.author}\nMessage supprimé par : ${deleter}\nMessage supprimé dans : ${message.channel}\nContenu du message : ${message.content}`)
  .setTimestamp()

  if (message.attachments.first()) {
    let attachment = message.attachments.first()

    if (attachment.url.endsWith('.png') || attachment.url.endsWith('.jpg') || attachment.url.endsWith('.gif')) {
      embed.setImage(attachment.url)
      console.log('image set')
    }
  }

  let channel = client.channels.cache.get(client.settings.logsChannelId);

  if (!channel) return;

  channel.send(embed)
}


/*
    This file is part of It-Hub Bot.

    It-Hub Bot is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    It-Hub Bot is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with It-Hub Bot.  If not, see <https://www.gnu.org/licenses/>.

*/
